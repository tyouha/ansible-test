nginx
=====

free, open-source, high-performance HTTP server and reverse proxy, as well as an IMAP/POP3 proxy server

Supported Tasks
-----------------

The following tasks are executed :

  - Install [nginx](http://nginx.org/)
  - Setup minimal settings using nginx.conf and defaut enabled website
  - Enable PHP-FPM proxy and copy a simple phpinfo script